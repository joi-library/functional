/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.functional;

/**
 * Represents a binary operator. It is an object that accepts to objects and 
 * returns a single object as result, that relates to the given objects.
 * This class should mostly be used for number operators such as '+'.
 * 
 * @author Elwin Slokker
 * @param <FirstInputT>
 * @param <SecondInputT>
 * @param <OutputT>
 */
public interface Operator<FirstInputT, SecondInputT, OutputT>
{
    /**
     * Evaluate the two arguments according to the implementation.
     * WARNING: Not every implementation has to be commutative.
     * 1 * 3 = 3  and 3 * 1 = 3 but this rule of 'reflection' does not always
     * apply.
     * 
     * @param firstOperand The first operand.
     * @param secondOperand The second operand.
     * @return The result of the operator with the given operands.
     */
    public abstract OutputT process(FirstInputT firstOperand, 
                                    SecondInputT secondOperand);
}
