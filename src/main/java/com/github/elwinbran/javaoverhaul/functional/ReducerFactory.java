/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.functional;

/**
 *
 * @author Elwin Slokker
 */
public interface ReducerFactory
{
    /**
     * 
     * @param <ReturnT>
     * @param <ElementT>
     * @param operator
     * @return 
     */
    public abstract <ReturnT, ElementT> Reducer<ReturnT, ElementT>
        makeFromOperator(Operator<ElementT, ReturnT, ReturnT> operator);
    
    /**
     * 'Chains' a {@link Chain} and a {@link Reducer} to produce a reducer.
     * 
     * @param <InputElementT>
     * @param <OutputElementT> The produced type of the {@code chain} and input
     * of the {@code reducer}.
     * @param <ReturnT>
     * @param reducer
     * @param chain
     * @return A reducer that first processes the elements through the 
     * {@code chain} and then returns the result that is produced from the {@code reducer}.
     */
    public abstract <InputElementT, OutputElementT, ReturnT>
        Reducer<ReturnT, InputElementT> 
        make(Reducer<OutputElementT, ReturnT> reducer, 
             Chain<InputElementT, OutputElementT> chain);
}
