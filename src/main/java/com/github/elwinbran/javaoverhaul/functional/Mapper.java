/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.functional;

/**
 * Extends default Function definition with extra support for an set of inputs.
 * A mapper function 'transforms' an ADT. This does not include changing the 
 * amount of elements; the input and output ADT's are exactly the same size. 
 * 
 * For removal, use a {@see Filter}.
 * 
 * @author Elwin Slokker
 * @param <InputElementT>
 * @param <OutputElementT>
 */
@FunctionalInterface
public interface Mapper<InputElementT, OutputElementT>
{   
    /**
     * 'Map' the given ADT onto a new ADT by transforming every element.
     * 
     * @param container
     * @return 
     */
    public abstract Iterable<OutputElementT> map(Iterable<InputElementT> container);
}
