/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.functional;

/**
 * Builds chains by adding chains together (linking).
 * To create {@link Chain Chain objects} use {@link ChainFactory}.
 * 
 * @author Elwin Slokker
 */
public interface ChainBuilder
{
    /**
     * Links to chains into a new chain.
     * 
     * @param <InputElementT> The element type that goes into the chain.
     * @param <OldOutputElementT> The element type that passed between the chains.
     * @param <NewOutputElementT> The element type that appears as result of 
     * chain processing.
     * @param inputChain The chain that comes first.
     * @param outputChain The chain that comes last.
     * @return A new composite chain that first processes {@code inputChain} and
     * passes that result to {@code outputChain} for the final result processing.
     */
    public abstract <InputElementT, OldOutputElementT, NewOutputElementT> 
        Chain<InputElementT, NewOutputElementT> build(
                Chain<InputElementT, OldOutputElementT> inputChain,
                Chain<OldOutputElementT, NewOutputElementT> outputChain);
    
}
